Project Title
This console program loads the JSON file and finds the overlapped rectangle.

Getting Started

	1. create a json file with below format:
		{
			"rects": [
				{"x": 100, "y": 100, "w": 250, "h": 80 },
				{"x": 120, "y": 200, "w": 250, "h": 150 },
				{"x": 140, "y": 160, "w": 250, "h": 100 },
				{"x": 160, "y": 140, "w": 350, "h": 190 }
			]
		}
		
		or call method to generate the json file: int generateJson(string &path)
		
	2. load the json file into the program with method: 
		int readJson(string &path, vector<Rect> &rectList)
	
	3. find the overlap among all the loaded rectangles:
	    void findIntersection(vector<Rect> rectList)
	
    Please note: int main() method can automatically finish the above steps.
	

Prerequisites:
	Please install IDE like eclipse/visual studio with standard C++; or simple gcc compiler
	

Examples
	Input:
	1: Rectangle at (100,100), w=250, h=80.
	2: Rectangle at (120,200), w=250, h=150.
	3: Rectangle at (140,160), w=250, h=100.
	4: Rectangle at (160,140), w=350, h=190.
	Intersections
	Between rectangle 1 and 3 at (140,160), w=210, h=20.
	Between rectangle 1 and 4 at (160,140), w=190, h=40.
	Between rectangle 2 and 3 at (140,200), w=230, h=60.
	Between rectangle 2 and 4 at (160,200), w=210, h=130.
	Between rectangle 3 and 4 at (160,160), w=230, h=100.
	Between rectangle 1, 3 and 4 at (160,160), w=190, h=20.
	Between rectangle 2, 3 and 4 at (160,200), w=210, h=60.

