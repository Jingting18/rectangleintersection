
/*
A program that reads the json file and finish the following tasks:
- print all the loaded rectangles
- print all the intersections among the rectangles 

the program reads the json with the following format:

{
	"rects": [
		{"x": 100, "y": 100, "w": 250, "h": 80 },
		{"x": 120, "y": 200, "w": 250, "h": 150 },
		{"x": 140, "y": 160, "w": 250, "h": 100 },
		{"x": 160, "y": 140, "w": 350, "h": 190 }
	]
}

Author:  Jingting Zeng
Date: 2/26/2017
*/
#include <iostream>
#include <fstream>
#include <string>
#include <set>
#include <vector>
#include <algorithm>
using namespace std;

class OverlapRectangle
{
private:
	const int LEN = 10;
	const int RECT_NUMBER = 50;
	const int MIN_RANGE = 1;
	const int MAX_RANGE = 500;
	const int INITIAL_PARENT_SIZE = 1;
public:
	//rectangle which is positioned by upperleft x, y and lowerright x, y position.
	struct Rect{
		int x1;
		int y1;
		int x2;
		int y2;
		set<int> parent;
	};

	//operator to sort the rectangle according to the lower left x value.
	struct less_than_key
	{
		inline bool operator() (const Rect& rect1, const Rect& rect2)
		{
			return (rect1.x1 < rect2.x1);
		}
	};

	//get the number from the word starting from index start.
	int getNum(string word, int& start){
		start = word.find(':', start) + 1;
		int end = word.find(',', start);
		string num = word.substr(start, end - start);
		int res = stoi(num);
		return res;
	}
	//print the rectangle intersection
	void printRectIntersection(vector<Rect> updatedRectList){
		for (size_t i = 0; i < updatedRectList.size(); i++){
			Rect rect = updatedRectList.at(i);
			string str;
			set<int> parentSet = rect.parent;
			set<int>::iterator it, it2ndLast;

			for (it = parentSet.begin(); it != parentSet.end(); ++it)
			{
				str.append(to_string(*it));
				str.append(", ");
			}
			str = str.substr(0, str.size() - 2);
			cout << "      Between rectangle " << str << " at (" << rect.x1 << "," << rect.y1 << "), w=" << rect.x2 - rect.x1 << ", h=" << rect.y2 - rect.y1 << "." << endl;
		}
	}


	/*
	Read the file containing rectangle in json format
	*/
	int readJson(string &path, vector<Rect> &rectList) {
		ifstream in(path);
		string word;

		int counter = 1;


		if (in.is_open()) {
			while (getline(in, word)) {
				if (word.length() > LEN)
				{
					Rect rect;
					int start = 0;
					rect.x1 = getNum(word, start);
					rect.y1 = getNum(word, start);
					rect.x2 = rect.x1 + getNum(word, start);
					rect.y2 = rect.y1 + getNum(word, start);
					set<int> rectParent;
					rectParent.insert(counter++);
					rect.parent = rectParent;
					rectList.push_back(rect);
				}
				if (counter > RECT_NUMBER){
					break;
				}
			}
			in.close();
		}
		else {
			cout << "Cannot open the file!" << endl;
			return 1;
		}

		return 0;
	}

	//generate the random number between the range min and max
	int getRand(int min, int max){
		return min + (rand() % (int)(max - min + 1));
	}

	int generateJson(string &path){
		ofstream jsonfile;
		jsonfile.open(path);

		jsonfile << "{" << endl;
		jsonfile << "\"rects\":[" << endl;
		for (int i = 0; i < RECT_NUMBER - 1; i++){
			jsonfile << "{\"x\": " << getRand(MIN_RANGE, MAX_RANGE) << ", \"y\": " << getRand(MIN_RANGE, MAX_RANGE) << ", \"w\": " << getRand(MIN_RANGE, MAX_RANGE) << ", \"h\": " << getRand(MIN_RANGE, MAX_RANGE) << "}, " << endl;
		}
		jsonfile << "{\"x\": " << getRand(MIN_RANGE, MAX_RANGE) << ", \"y\": " << getRand(MIN_RANGE, MAX_RANGE) << ", \"w\": " << getRand(MIN_RANGE, MAX_RANGE) << ", \"h\": " << getRand(MIN_RANGE, MAX_RANGE) << "}" << endl;
		jsonfile << "]" << endl;
		jsonfile << "}" << endl;
		jsonfile.close();
		return 0;
	}

	// print the loaded rectangle
	void printRect(vector<Rect> rectList){
		for (size_t i = 0; i < rectList.size(); i++){
			Rect rect = rectList[i];
			cout << "      " << i + 1 << ": Rectangle at (" << rect.x1 << "," << rect.y1 << "), w=" << rect.x2 - rect.x1 << ", h=" << rect.y2 - rect.y1 << "." << endl;
		}
	}


	//get the intersection of Rectangle a and b;  a is ploted by two points(aX1, aY1), (aX2, aY2);
	//b is ploted by two points(bX1, bY1), (bX2, bY2)
	bool computeArea(Rect a, Rect b, Rect &rect) {
		if (b.x1 >= a.x2 || b.y1 >= a.y2 || a.y1 >= b.y2 || a.x1 >= b.x2) {
			return false;
		}
		rect.x1 = max(a.x1, b.x1);
		rect.y1 = max(a.y1, b.y1);
		rect.x2 = min(a.x2, b.x2);
		rect.y2 = min(a.y2, b.y2);

		rect.parent.insert(a.parent.begin(), a.parent.end());
		rect.parent.insert(b.parent.begin(), b.parent.end());
		return true;
	}

	//check whether the rect is already intersection by others
	int findRect(vector<Rect> updatedRectList, Rect rect){
		for (size_t i = 0; i < updatedRectList.size(); i++){
			Rect current = updatedRectList[i];
			if (current.x1 == rect.x1 && current.y1 == rect.y1 && current.x2 == rect.x2 && current.y2 == rect.y2 && current.parent == rect.parent)
			{
				return i;
			}
		}
		return -1;
	}

	//find the intersections among the rectangles;
	//the algorithm sorts the rectangle according to the lowerleft x value
	//it only searches the rectangle with x value less than current x value. 
	void findIntersection(vector<Rect> rectList){
		if (rectList.size() == 0)
		{
			return;
		}
		//loop the rectangle and find the intersection.
		vector<Rect> updatedRectList;
		std::sort(rectList.begin(), rectList.end(), less_than_key());
		for (size_t i = 1; i < rectList.size(); i++){
			for (size_t j = 0; j < i; j++){

				//find the intersection of two rectangle;
				Rect a = rectList[i];
				Rect b = rectList[j];
				Rect rect;
				if (computeArea(a, b, rect)){
					rect.parent.insert(a.parent.begin(), a.parent.end());
					rect.parent.insert(b.parent.begin(), b.parent.end());
					//if the found intersection satisfies the below creteria, insert it to the updated rectangle intersection list.
					//  1. NOT one of the existing input rectangle,  
					//  2. NOT one of the found rectangle intersection,
					//
					if (findRect(updatedRectList, rect) < 0 && findRect(rectList, rect) < 0){
						updatedRectList.push_back(rect);
					}
				}
			}
		}
		//print the result for the intersection of rectangle; save the result into updatedRectList
		printRectIntersection(updatedRectList);
		//find another layer of intersection from the updatedRectList
		findIntersection(updatedRectList);
		return;
	}
};


#ifndef RunTests
int main() {
	//test set 1; original test case with 4 regular rectangle
	string rectJson1 = "./rect.json";

	//test set 2; dynamically generating json file with any size of rectangle. The size is set by RECT_NUMBER
	string rectJson2 = "./rect1000.json";
	//generateJson(rectJson2);

	//test set 3; four overlapped rectangle;
	string rectJson3 = "./rect-repeat1.json";

	//test set 4; three overlapped rectangle with one different rectangle;
	string rectJson4 = "./rect-repeat2.json";

	vector<OverlapRectangle::Rect> rectList;
	cout << "Input:" << endl;
	OverlapRectangle overlapRect;
	overlapRect.readJson(rectJson1, rectList);
	overlapRect.printRect(rectList);
	cout << "Intersections" << endl;
	overlapRect.findIntersection(rectList);
	return 0;
}
#endif